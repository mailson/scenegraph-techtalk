import QtQuick 2.0
import Example 1.0

Item {
    width: 400
    height: 300

    MySGElement {
        width: 40
        height: 40

        color: "red"

        anchors.centerIn: parent
    }
}
