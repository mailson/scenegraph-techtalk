#include "mysgelement.h"
#include <private/qsgadaptationlayer_p.h>
#include <private/qquickitem_p.h>

MySGElement::MySGElement(QQuickItem *parent)
    : QQuickItem(parent)
{
    setFlag(ItemHasContents);
}

QColor MySGElement::color() const
{
    return m_color;
}

void MySGElement::setColor(const QColor &color)
{
    if (color == m_color)
        return;

    m_color = color;
    emit colorChanged();
    update();
}

QSGNode *MySGElement::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data)
{
    Q_UNUSED(data);

    if (width() <= 0 || height() <= 0) {
        delete oldNode;
        return 0;
    }

    QSGRectangleNode *rectangle = static_cast<QSGRectangleNode*>(oldNode);
    if (!rectangle) rectangle = QQuickItemPrivate::get(this)->sceneGraphContext()->createRectangleNode();

    rectangle->setRect(QRectF(0,0,width(),height()));
    rectangle->setColor(m_color);
    rectangle->update();
    return rectangle;
}
