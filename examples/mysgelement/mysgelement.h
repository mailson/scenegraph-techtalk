#ifndef MYSGELEMENT_H
#define MYSGELEMENT_H

#include <QtQuick/QQuickItem>

class MySGElement : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
public:
    MySGElement(QQuickItem *parent = 0);

    QColor color() const;
    void setColor(const QColor &);

signals:
    void colorChanged();

protected:
    virtual QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);

private:
    QColor m_color;
};

#endif // MYSGELEMENT_H
