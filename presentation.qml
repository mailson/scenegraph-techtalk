import QtQuick 1.1
import "quickshow"

Presentation {
    id: root

    width: 1024
    height: 768

    enableMouse: false

    ImageSlide {
        id: cover

        source: "resources/cover.png"
        title: "Qt SceneGraph 101"
        authors: "Mailson D. Lira Menezes"
        date: "27 de Abril de 2012"
    }

     // RegularSlide {
     //     title: "Agenda"

     //     content: [
     //         "The easy way"
     //     ]
     // }

     RegularSlide {
         title: "Why?"
         content: [
             "Improve Qt OpenGL graphics system performance",
             "Step back",
             "Different model: scene graph"
         ]
     }

     RegularSlide {
         title: "Change what needs to be changed"
         content: [
             "Textures",
             "Matrices",
             "Colors",
             "Geometry",
             "..."
         ]
     }

     RegularSlide {
         title: "Qt scene graph"
         content: [
             "A tree of predefined node types",
             "Populate with geometry nodes",
             "Geometry node: vertices+material",
             "Renderer is free to reorganize the tree", // Material type
             "z-buffer" // Translucent, opaque
         ]
     }

     RegularSlide {
         title: "A tree of predefined node types"
         content: [
             "QSGNode",
             "QSGRootNode",
             "QSGGeometryNode",
             "QSGTransformNode",
             "QSGClipNode"
         ]
     }

     RegularSlide {
         title: "QSGGeometryNode"
         content: [
             "Used for all visible content",
             "Material",
             "Geometry"
         ]
     }

     DividerSlide {
         centeredText: "Show me the code"
     }

     RegularSlide {
         property alias __showRect: easywayRect.visible
         title: "The easy way"

         Item {
             id: easywayRect

             visible: false

             anchors {
                 left: parent.left
                 right: easywayCode.left
                 top: parent.top
                 bottom: parent.bottom
             }

             Rectangle {
                 anchors.centerIn: parent
                 width: 40
                 height: 40
                 color: "#ff0000"
             }
         }

         CodeSection {
             id: easywayCode

             text: '
import QtQuick 2.0
Rectangle {
    width: 40
    height: 40
    color: "#ff0000"
}
             '

             MouseArea {
                 anchors.fill: parent
                 onClicked: {
                     easywayRect.visible = true;
                 }
             }
         }
     }

     RegularSlide {
         title: "QQuickItem"

         TitledCodeSection {
             anchors.fill: parent

             title: "updatePaintNode"

             text: '
QSGNode *MySGElement::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data)
{
    QSGRectangleNode *rectangle = new QSGDefaultRectangleNode;
    rectangle->setRect(QRectF(0,0,width(),height()));
    rectangle->setColor("#ff0000");
    rectangle->update();

    return rectangle;
}
             '
         }
     }

     RegularSlide {
         title: "Creating your first SG node"
         content: [
             "MySGNode",
             "Must have a material",
             " QSGFlatColorMaterial",
             "A geometry",
             " QSGGeometry",
             "And some properties",
             " rect",
             " color"
         ]
     }

     RegularSlide {
         title: "Creating your first SG node"

         TitledCodeSection {
             anchors.fill: parent

             title: "mysgnode.h"
             text: '
class MySGNode : public QSGGeometryNode
{
public:
    MySGNode();

    void setRect(const QRectF &rect);
    void setColor(const QColor &color);
    void update();
private:
    void updateGeometry();

    QRectF m_rect;
    QSGGeometry m_geometry;
    QSGFlatColorMaterial m_material;
    uint m_dirtyGeometry : 1;
};
             '
         }
     }

     RegularSlide {
         title: "Creating your first SG node"

         TitledCodeSection {
             anchors {
                 left: parent.left
                 right: parent.right
             }

             height: parent.height / 2
             title: "mysgnode.cpp"
             text: '
MySGNode::MySGNode()
    : m_dirtyGeometry(false)
    , m_geometry(QSGGeometry::defaultAttributes_Point2D(), 4)
{
    setGeometry(&m_geometry);
    setMaterial(&m_material);
}
             '
         }

         CodeSection {
             anchors {
                 bottom: parent.bottom
                 left: parent.left
                 right: parent.right
             }

             height: parent.height/2.2
             text: '
QSGGeometry(AttributeSet & attributes, int vertexCount...)
             '
         }
     }

     RegularSlide {
         title: "Creating your first SG node"

         CodeSection {
             anchors.fill: parent

             text: '
void MySGNode::setRect(const QRectF &rect)
{
    if (rect == m_rect)
        return;

    m_rect = rect;
    m_dirtyGeometry = true;
}

void MySGNode::setColor(const QColor &color)
{
    if (color == m_material.color())
        return;

    m_material.setColor(color);
    markDirty(DirtyMaterial);
}
             '
         }
     }

     RegularSlide {
         CodeSection {
             anchors.fill: parent

             text: '
void MySGNode::update()
{
    if (m_dirtyGeometry) {
        updateGeometry();
        m_dirtyGeometry = false;
    }
}

void MySGNode::updateGeometry()
{
    QSGGeometry::updateRectGeometry(&m_defaultGeometry, m_rect);
    markDirty(DirtyGeometry);
}
             '
         }
     }
}
