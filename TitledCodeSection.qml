import QtQuick 1.1
import "quickshow"

Item {
    property alias title: codeTitle.text
    property alias text: codeSection.text
    property real baseFontSize: parent.baseFontSize // XXX

    x: parent.width / 2
    width: parent.width / 2
    height: parent.height

    Text {
        id: codeTitle
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }

        font.pixelSize: parent.baseFontSize
        color: "white"
    }

    CodeSection {
        id: codeSection

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: codeTitle.bottom
            topMargin: 10
        }
    }
}
